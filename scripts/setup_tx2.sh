#!/usr/bin/env bash

SOURCE_DIR=/data/git/miner-explorer/tx2_root
ARG_DIFF=false
DIFF_TOOL=diff

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"
#echo "Key:$key"
case $key in
    -d|--diff)
    ARG_DIFF="true"
    shift # past argument
    ;;
    #-s|--searchpath)
    #SEARCHPATH="$2"
    #shift # past argument
    #shift # past value
    #;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    echo "Invalid argument $key"
    exit -1
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters


# file:action:
SOURCE_INFO="
/bin/miner-rosrun-maplab.sh:override:
/bin/miner-wait-ros-topic.sh:override:
/bin/miner-roslaunch.sh:override:
/bin/miner-print-env.sh:override:
/bin/miner-roscore.sh:override:
/bin/miner-env-maplab.sh:override:
/bin/miner-roslaunch-maplab.sh:override:
/bin/miner-env.sh:override:
/lib/systemd/system/miner-msf.service:override:
/lib/systemd/system/miner-loitor.service:override:
/lib/systemd/system/miner-dji_sdk.service:override:
/lib/systemd/system/miner-mavros.service:override:
/lib/systemd/system/miner-zed.service:override:
/lib/systemd/system/miner-rovioli.service:override:
/lib/systemd/system/miner-tegra.service:override:
/lib/systemd/system/miner-control.service:override:
/lib/systemd/system/miner-record.service:override:
/lib/systemd/system/miner-vins.service:override:
/lib/systemd/system/miner-roscore.service:override:
/lib/systemd/system/miner-transforms.service:override:
/etc/dnsmasq.conf:override:
/etc/sysctl.conf:override:
/etc/udev/rules.d/70-persistent-uart.rules:override:
/etc/hostapd/hostapd.conf:override:
/etc/resolv.conf:override:
/etc/modprobe.d/bcmdhd.conf:override:
"

function ProcessFile {
	local FILE="$1"
	local ACTION="$2"
	#echo "Action : $ACTION; for: $FILE"
	
	case $ACTION in
	override)
	  OverrideFile $FILE
	  ;;
	*)
	  Message="Invalid ACTION: $ACTION"
	  ;;
	esac
}

function OverrideFile {
	local FILE="$1"
	echo "OVERRIDE: ${SOURCE_DIR}${FILE} >> $FILE"
	sudo cp -a ${SOURCE_DIR}${FILE} $FILE
}

if [ "$ARG_DIFF" = true ]; then
	echo "$SOURCE_INFO" | while read LINE; do
	if [ ! -z "$LINE" ]; then
		FILE=$(echo "$LINE" | cut -f 1 -d ':')
		NEW_FILE=${SOURCE_DIR}${FILE}
		if [ -f $FILE ] && [ -f $NEW_FILE ] ;then		
			if ! cmp -s "$FILE" "$NEW_FILE"
			then
				$DIFF_TOOL --side-by-side $FILE $NEW_FILE
			fi
		fi
		
	fi
	done
else
	echo "$SOURCE_INFO" | while read LINE; do
	if [ ! -z "$LINE" ]; then
		FILE=$(echo "$LINE" | cut -f 1 -d ':')
		ACTION=$(echo "$LINE" | cut -f 2 -d ':')
		ProcessFile $FILE $ACTION
	fi
	done    
fi

