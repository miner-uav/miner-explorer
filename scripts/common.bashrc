
#helpful shortcuts
alias cde='cd /data/git/miner-explorer'
alias cdm='cd /data/git/miner-tx1/catkin_ws;. devel/setup.bash'
alias cdp='cd /data/git/miner-maplab;. devel/setup.bash'
alias cdn='cd /data/git/miner-nbv;. devel/setup.bash'

# added by Anaconda3 installer
alias ac='export PATH="/data/tools/anaconda3/bin:$PATH"'
alias jpt_python='PATH="/data/tools/anaconda3/bin:$PATH";jupyter notebook --notebook-dir=/data/Documents/Python-Data-Science-and-Machine-Learning-Bootcamp'
alias jpt_tf='PATH="/data/tools/anaconda3/bin:$PATH";jupyter notebook --notebook-dir=/data/Documents/Tensorflow-Bootcamp-master'

# git aliases
alias gs='git status'

alias dk='docker'

alias sysdi='sudo /data/git/miner-explorer/scripts/systemd_install.py'

alias jenkins='export JENKINS_HOME=/data/tools/jenkins/.jenkins;java -jar /data/tools/jenkins/jenkins.war --httpPort=8080'

# Repositories
export PATH=/data/git/miner-gc/scripts:$PATH
export PATH=/data/git/miner-explorer/scripts:$PATH
export PATH=/data/git/miner-explorer/python:$PATH

#Tools
export PATH="/data/tools/pycharm-community/bin${PATH:+:${PATH}}"
export PATH=/data/tools/cmake-3.12.0-Linux-x86_64/bin:$PATH


# Qtcreator with ROS plugin
#export PATH=/opt/Qt/Tools/QtCreator/bin:$PATH
export PATH=${HOME}/QtCreator/latest/bin:$PATH

# Ninja build system
export PATH=/data/tools/ninja:$PATH


#PX4 build
#export PATH=/data/tools/ninja:$PATH
#export PATH=/data/tools/gcc-arm-none-eabi-5_4-2016q2/bin:$PATH

export PATH="/usr/local/cuda-9.1/bin${PATH:+:${PATH}}"
export LD_LIBRARY_PATH="/usr/local/cuda-9.1/lib64${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}"


# installed during rovioli https://github.com/ethz-asl/maplab/wiki/Installation-Ubuntu
export PATH="/usr/lib/ccache${PATH:+:${PATH}}"

. /opt/ros/kinetic/setup.bash

. /data/git/linter/setup_linter.sh

export JENKINS_HOME=/data/tools/jenkins/.jenkins
