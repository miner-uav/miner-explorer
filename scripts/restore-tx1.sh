#!/usr/bin/env bash

BKP_SOURCE=/media/ubuntu/SD64G/system-tx1/
BKP_DESTINATION=/nvme0n1p1

if [[ -d  $1 ]]; then
	BKP_DESTINATION=$1
fi

echo "Destination: $BKP_DESTINATION" 

rsync -WaxHAXv --numeric-ids --info=progress2 --delete \
  $BKP_SOURCE $BKP_DESTINATION 
