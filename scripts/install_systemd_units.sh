#!/usr/bin/env bash


if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi
HOST_NAME=`hostname`



case "$HOST_NAME" in
        ubuntu-pathfinder)
            ROOT_PATH=/data/git/miner-explorer/pathfinder_root
            ;;
        maciej-XPS-15-9550)
            ROOT_PATH=/data/git/miner-explorer/pathfinder_root
            ;;
         
        tegra-ubuntu)
            ROOT_PATH=/data/git/miner-explorer/tx2_root
            ;;
        *)
            echo "Host $HOST_NAME is not configured!!!"
            exit 1
 
esac

case "$1" in
        prod)
            export SERVICES_AUTO='miner-roscore.service miner-diagnostic.service miner-transforms.service  miner-dji_sdk.service miner-loitor.service miner-rovioli.service miner-msf.service miner-control.service miner-laser.service '
            ;;

        local)
            export SERVICES_AUTO='miner-roscore.service miner-diagnostic.service miner-transforms.service  miner-rovioli.service miner-msf.service miner-control.service miner-transforms.service '
            ;;
         
        minimal)
            export SERVICES_AUTO='miner-roscore.service miner-diagnostic.service miner-transforms.service '
            ;;
         
        loitor)
            export SERVICES_AUTO='miner-roscore.service miner-diagnostic.service miner-transforms.service miner-loitor.service '
            ;;
            
        rovioli)
            export SERVICES_AUTO='miner-roscore.service miner-diagnostic.service miner-transforms.service miner-loitor.service miner-rovioli.service miner-msf.service miner-dji_sdk.service '
            ;;
            
        none)
            export SERVICES_AUTO=''
            ;;
            
        enable)
            export SERVICES_AUTO='enable'
            ;;
            
        disable)
            export SERVICES_AUTO='disable'
            ;;
            
        *)
            echo $"Usage: $0 {prod|local|minimal|loitor|rovioli|none}"
            exit 1
 
esac

if [[ $SERVICES_AUTO == 'enable' ]];then
  echo "Enabling Service $2"
  SERVICE_NAME="miner-${2}.service"
  if [[ -f /lib/systemd/system/${SERVICE_NAME} ]]; then
    sudo systemctl enable ${SERVICE_NAME}
    sudo systemctl start ${SERVICE_NAME}
  else
    echo "service ${SERVICE_NAME} does not exists"
  fi
  exit 0
fi

if [[ $SERVICES_AUTO == 'disable' ]];then
  echo "Disabling Service $2"
  SERVICE_NAME="miner-${2}.service"
  if [[ -f /lib/systemd/system/${SERVICE_NAME} ]]; then
    sudo systemctl stop ${SERVICE_NAME}
    sudo systemctl enable ${SERVICE_NAME}
  else
    echo "service ${SERVICE_NAME} does not exists"
  fi
  exit 0
fi

echo "Using root    :$ROOT_PATH"
echo "Using config  :$1"
echo "Auto services :$SERVICES_AUTO"

export SERVICES_ALL='miner-roscore.service miner-record.service miner-dji_sdk.service miner-transforms.service miner-loitor.service miner-stereo.service miner-rovioli.service miner-msf.service miner-control.service '

echo "Stoping services..."
sudo systemctl stop $SERVICES_ALL
echo "Disabling services..."
sudo systemctl disable $SERVICES_AUTO $SERVICES_ALL

echo "Removing old scripts..."
sudo rm -rf /bin/miner-*
sudo rm -rf /lib/systemd/system/miner-*

echo "coping new scripts..."
sudo cp ${ROOT_PATH}/bin/* /bin/
sudo cp ${ROOT_PATH}/lib/systemd/system/* /lib/systemd/system/
sudo chmod +x /bin/miner-*

echo "Reloading systemctl daemon configuration.."
sudo systemctl daemon-reload 
if ! [[ -z "${SERVICES_AUTO// }" ]]; then
	echo "Enabling Auto services..."
	sudo systemctl enable $SERVICES_AUTO
	echo "Starting Auto services..."
	sudo systemctl start $SERVICES_AUTO
else
	echo "No auto services configured."
fi

echo "Running services:"
systemctl list-units | grep miner
