#!/usr/bin/env bash
KALIBR_WS=/home/maciej/kalibr_workspace

TOPIC_LEFT=/loitor_stereo_visensor/left/image_raw
TOPIC_RIGHT=/loitor_stereo_visensor/right/image_raw
BAGS_FOLDER=/data/rosbag/kalibr
BAG_FILE=/static_2018-02-05-11-34-08.bag

. $KALIBR_WS/devel/setup.bash


kalibr_calibrate_cameras --target april_6x6.yaml \
--bag ${BAGS_FOLDER}${BAG_FILE} \
--models pinhole-radtan pinhole-radtan \
--topics ${TOPIC_LEFT} ${TOPIC_RIGHT} \
--show-extraction


#rosrun kalibr kalibr_maplab_config \
#--to-ncamera \
#--label cam_label \
#--cam /data/git/miner-explorer/scripts/kalibr/camchain-datarosbagkalibrstatic_2018-02-01-15-55-42.yaml
