#!/usr/bin/env bash
KALIBR_WS=/home/maciej/kalibr_workspace

TOPIC_LEFT=/loitor_stereo_visensor/left/image_raw
TOPIC_RIGHT=/loitor_stereo_visensor/right/image_raw
BAGS_FOLDER=/data/rosbag/kalibr
BAG_FILE=/dynamic_production_exp10_take2
BAG_TIME_FROM=4.5
BAG_TIME_TO=29
CAM_YAML=camchain-datarosbagkalibrstatic_2018-02-05-11-34-08.yaml
IMU_YAML=imu_MPU6050.yaml

. $KALIBR_WS/devel/setup.bash


kalibr_calibrate_imu_camera \
--target april_6x6.yaml \
--bag ${BAGS_FOLDER}${BAG_FILE}.bag \
--imu $IMU_YAML \
--cam $CAM_YAML \
--show-extraction \
--time-calibration \
--bag-from-to $BAG_TIME_FROM $BAG_TIME_TO


rosrun kalibr kalibr_maplab_config \
--to-ncamera \
--label cam_label \
--cam /data/git/miner-explorer/scripts/kalibr/camchain-imucam-datarosbagkalibrdynamic_2018-02-01-16-21-42.yaml
