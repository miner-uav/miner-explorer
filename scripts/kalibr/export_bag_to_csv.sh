#!/usr/bin/env bash

TOPIC_IMU=/loitor_stereo_visensor/imu0
BAG_FILE_NAME=/data/rosbag/noise/imu_noise.bag

rosbag record -O $BAG_FILE_NAME $TOPIC_IMU

rostopic echo -b $BAG_FILE_NAME -p $TOPIC_IMU > $BAG_FILE_NAME.csv

