#!/usr/bin/env bash
KALIBR_WS=/home/maciej/kalibr_workspace

CAM_CHAIN_IMU=$1
CAM_LABEL=$2
CAM_YAML=camchain-datarosbagkalibrstatic_2018-02-05-11-34-08.yaml
IMU_YAML=imu_MPU6050.yaml

. $KALIBR_WS/devel/setup.bash


rosrun kalibr kalibr_maplab_config \
--label ${CAM_LABEL} \
--imu ${IMU_YAML} \
--cam ${CAM_CHAIN_IMU}
