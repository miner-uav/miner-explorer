#!/usr/bin/env bash

BAG_ID=$1
TOPIC_LEFT=/loitor_stereo_visensor/left/image_raw
TOPIC_RIGHT=/loitor_stereo_visensor/right/image_raw
TOPIC_IMU=/loitor_stereo_visensor/imu0


rosbag record -O /data/rosbag/kalibr/dynamic_${BAG_ID}.bag $TOPIC_LEFT $TOPIC_RIGHT $TOPIC_IMU

