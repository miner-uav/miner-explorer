#!/usr/bin/env python
import pwd
import logging
import grp
import dbus
import stat
import socket
import argcomplete
import argparse
import subprocess
import os
import glob
import shutil


# dbus lib: https://wiki.freedesktop.org/www/Software/systemd/dbus/

def prompt_sudo():
    ret = 0
    if os.geteuid() != 0:
        msg = "[sudo] password for %u:"
        ret = subprocess.check_call("sudo -v -p '%s'" % msg, shell=True)
    return ret

class SystemdHelper:
    def __init__(self, source_files):
        self.hostName = socket.gethostname()
        self.svc_path = "/lib/systemd/system"
        if not source_files :
            tempPath = os.path.dirname(os.path.realpath(__file__))
            if self.hostName == 'ubuntu-pathfinder':
                tempPath = os.path.join(tempPath, os.path.pardir, "pathfinder_root")
            elif self.hostName == 'maciej-XPS-15-9550':
                tempPath = os.path.join(tempPath, os.path.pardir, "pathfinder_root")
            elif self.hostName == 'tegra-ubuntu':
                tempPath = os.path.join(tempPath, os.path.pardir, "tx2_root")
            else:
                raise SystemError("!!!!!!!!!!!!!!!!!!!!!! Unknown machine !!!!!!!!!!!!!!!!!!!!!!")
            self.source_files = os.path.realpath(tempPath)
        else:
            self.source_files = source_files
        sysbus = dbus.SystemBus()
        systemd1 = sysbus.get_object('org.freedesktop.systemd1', '/org/freedesktop/systemd1')
        self.sysctl = dbus.Interface(systemd1, 'org.freedesktop.systemd1.Manager')

    def refreshFiles(self):
        modebin = stat.S_IRWXU | stat.S_IRGRP | stat.S_IXGRP | stat.S_IROTH | stat.S_IXOTH
        modesvc = stat.S_IRUSR | stat.S_IWUSR | stat.S_IRGRP | stat.S_IROTH
        root_uid = pwd.getpwnam("root").pw_uid
        root_gid = grp.getgrnam("root").gr_gid

        for file in glob.glob('/bin/miner*'):
            logging.debug("removing file: " + file)
            os.remove(file)

        for file in glob.glob('/lib/systemd/system/miner*'):
            logging.debug("removing file: " + file)
            os.remove(file)

        src = os.path.join(self.source_files, 'lib/systemd/system/miner*')
        for file in glob.glob(src):
            fname = os.path.basename(file)
            dst = os.path.join('/lib/systemd/system/', fname)
            logging.debug("coping " + file + " to " + dst)
            shutil.copyfile(file, dst)
            os.chmod(dst, modesvc)
            os.chown(dst, root_uid, root_gid)
        # copy bin files
        src = os.path.join(self.source_files, 'bin/miner*')
        for file in glob.glob(src):
            fname = os.path.basename(file)
            dst = os.path.join('/bin', fname)
            logging.debug("coping " + file + " to " + dst)
            shutil.copyfile(file, dst)
            os.chmod(dst, modebin)
            os.chown(dst, root_uid, root_gid)
        self.sysctl.Reload()
        pass


    def enableService(self, service_name):
        svc = self.reasonServiceName(service_name)
        if not self.checkServiceFileExists(svc):
            logging.error("file %s does not exists!!!")
            return
        status = self.sysctl.GetUnitFileState(svc)
        if status == 'enabled':
            logging.info("service " + svc + " already enabled")
            return
        else:
            logging.info("enabling service " + svc)
            self.sysctl.EnableUnitFiles([svc], False, True)
            return
        pass

    def disableService(self, service_name):
        svc = self.reasonServiceName(service_name)
        status = self.sysctl.GetUnitFileState(svc)
        if status == 'disabled':
            logging.info("service " + svc + " already disabled")
            return
        else:
            self.sysctl.DisableUnitFiles([svc], False)
            return

    def reasonServiceName(self, service_name):
        if service_name.endswith('.service'):
            svc = service_name
        else:
            svc = service_name + '.service'
        if not svc.startswith('miner-'):
            svc = 'miner-' + svc
        return svc

    def startService(self, service_name):
        svc = self.reasonServiceName(service_name)
        self.sysctl.StartUnit(svc, 'replace')
        return

    def stopService(self, servise_name):
        svc = self.reasonServiceName(service_name)
        self.sysctl.StopUnit(svc, 'replace')
        return

    def startEnabled(self):
        entries = self.getMinerServices()
        for key in entries:
            entry = entries[key]
            if entry['enabled'] == 'enabled' and not entry['status'] == 'active':
                logging.info("Starting Service: " + entry['file'])
                self.sysctl.StartUnit(entry['file'], 'replace')

    def stopRunning(self, stopRoscore):
        entries = self.getMinerServices()
        for key in entries:
            entry = entries[key]
            if entry['status'] == 'active':
                if entry['file'] == 'miner-roscore.service':
                    if stopRoscore:
                        self.sysctl.StopUnit(entry['file'], 'replace')
                else:
                    self.sysctl.StopUnit(entry['file'], 'replace')


    def getMinerServices(self):
        entries = {}
        for svc in self.sysctl.ListUnits():
            file = str(svc[0])
            if file.startswith('miner-'):
                name = str(svc[1])
                loaded = str(svc[2])
                status = str(svc[3])
                logging.debug("getMinerServices ListUnits adding:" + file)
                entries[file] = {
                    'file': file,
                    'desc': name,
                    'loaded': loaded,
                    'status': status,
                    'enabled': ''
                }
        for svc in self.sysctl.ListUnitFiles():
            file = os.path.basename(svc[0])
            if file.startswith('miner-'):
                if file in entries:
                    entries[file]['enabled'] = str(svc[1])
                else:
                    logging.debug("getMinerServices ListUnitFiles adding:" + file)
                    entries[file] = {
                        'file': file,
                        'desc': '',
                        'loaded': '',
                        'status': '',
                        'enabled': str(svc[1])
                    }
        return entries

    def printStatus(self):

        entries = self.getMinerServices()
        print("--------------------------------------------------------------------------------------------------------------------")
        print("| {0:30} | {1:40} | {2:8} | {3:8} | {4:14} |".format("File Name", "Description", "Enabled", "Loaded", "Status"))
        print("--------------------------------------------------------------------------------------------------------------------")
        for key in sorted(entries):
            entry = entries[key]
            print("| {0:30} | {1:40} | {2:8} | {3:8} | {4:14} |".format(entry['file'], entry['desc'], entry['enabled'], entry['loaded'], entry['status']))
        pass

    def cleanAll(self):
        svc_files = self.sysctl.ListUnits()
        for svc in svc_files:
            filename = svc[0]
            if filename.startswith("miner-"):
                self.disableService(filename)
        self.sysctl.Reload()
        for file in glob.glob('/bin/miner*'):
            print("removing file: " + file)
            os.remove(file)

        for file in glob.glob('/lib/systemd/system/miner*'):
            print("removing file: " + file)
            os.remove(file)
        pass

    def restartService(self, service_name):
        svc = self.reasonServiceName(service_name)
        self.sysctl.RestartUnit(svc, 'replace')
        pass

    def checkServiceFileExists(self, svc_file):
        fullPath = os.path.join(self.svc_path, svc_file)
        return os.path.isfile(fullPath)



# Main function.
if __name__ == '__main__':

#    if prompt_sudo() != 0:
#        # the user wasn't authenticated as a sudoer, exit?
#        exit(-1)

    # Initialize the node and name it.
    parser = argparse.ArgumentParser(description='install and maintains miner systemd scripts')
    parser.add_argument('-r', '--refresh',  action='store_true', help='refresh systemd files ')
    parser.add_argument('-v', '--verbose',  action='store_true', help='enable debug messages')
    parser.add_argument('-a', '--all',  action='store_true', help='flag augments behaviour of --stopa to include roscore task')
    parser.add_argument('--starta',  action='store_true', help='start all enabled miner services ')
    parser.add_argument('--start', metavar='service', help='start services ')
    parser.add_argument('--stopa', action='store_true', help='stop all miner services except roscore')
    parser.add_argument('--stop', metavar='service', help='stop service')
    parser.add_argument('-c', '--clean',  action='store_true', help='stops, disable and remove all miner- services')
    parser.add_argument('-e', '--enable', metavar='service', help='enable service')
    parser.add_argument('-d', '--disable', metavar='service', help='enable service')
    parser.add_argument('-res', '--restart', metavar='service', help='restart service service')
    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    sysdHelper = SystemdHelper("")

    if args.refresh:
        sysdHelper.refreshFiles()
    elif args.clean:
        sysdHelper.cleanAll()

    elif args.start:
        sysdHelper.startService(args.start)
    elif args.stop:
        sysdHelper.stopService(args.stop)

    elif args.starta:
        sysdHelper.startEnabled()
    elif args.stopa:
        sysdHelper.stopRunning(args.all)

    elif args.enable:
        sysdHelper.enableService(args.enable)
    elif args.disable:
        sysdHelper.disableService(args.disable)
    elif args.restart:
        sysdHelper.restartService(args.restart)

    sysdHelper.printStatus()
