#!/usr/bin/env bash

ARG=$1

show_help() {
    echo "This script sets up network interfaces"
    echo "Usage: $0 [SELECTION]"
    echo "    [SELECTION]: select WiFi / LAN config"
    echo "     - MG:MINE-GC"
    echo "     - M:MaciejAP"
    echo "     - L:LAN"
    echo "     - LMG:LAN + MINE-GC"
    exit 1
}

echo ARG1:$ARG

case $ARG in
	M)
		sudo ifdown eth0
		sudo ifdown wlan0
		sudo rm -rf /etc/network/interfaces.d/*
		sudo cp /data/git/miner-explorer/scripts/wlan0.MaciejAP /etc/network/interfaces.d/wlan0
		sudo ifup wlan0
		sudo ifup eth0
		;;
	MG)
		sudo ifdown eth0
		sudo ifdown wlan0
		sudo rm -rf /etc/network/interfaces.d/*
		sudo cp /data/git/miner-explorer/scripts/wlan0.MINE-GC /etc/network/interfaces.d/wlan0
		sudo ifup wlan0
		sudo ifup eth0
		;;
	L)
		sudo ifdown eth0
		sudo ifdown wlan0
		sudo rm -rf /etc/network/interfaces.d/*
		sudo cp /data/git/miner-explorer/scripts/eth0 /etc/network/interfaces.d/eth0
		sudo ifup wlan0
		sudo ifup eth0
		;;
	LMG)
		sudo ifdown eth0
		sudo ifdown wlan0
		sudo rm -rf /etc/network/interfaces.d/*
		sudo cp /data/git/miner-explorer/scripts/eth0 /etc/network/interfaces.d/eth0
		sudo cp /data/git/miner-explorer/scripts/wlan0.MINE-GC /etc/network/interfaces.d/wlan0
		sudo ifup wlan0
		sudo ifup eth0
		;;
	*)
		show_help
		;;
esac

echo Message:$Message
