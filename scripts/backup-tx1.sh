#!/usr/bin/env bash

BKP_SOURCE=/
BKP_DESTINATION=/media/ubuntu/SD64G/system-tx1/

if [[ -d  $1 ]]; then
	BKP_DESTINATION=$1
fi

echo "Destination: $BKP_DESTINATION" 

rsync -WaxHAXv --numeric-ids --info=progress2 --delete-excluded \
  $BKP_SOURCE $BKP_DESTINATION \
  --dry-run  \
  --exclude='/home/*/.gvfs' \
  --exclude='/home/*/.local/share/Trash' \
  --exclude='/var/run/*' \
  --exclude='/var/lock/*' \
  --exclude='/var/log/*' \
  --exclude='/var/cache/apt/archives/*' \
  --exclude='/lib/modules/*/volatile/.mounted' \
  --exclude='/home/*/.mozilla/firefox/*/Cache' \
  --exclude='/home/*/.cache/chromium' \
  --exclude='/home/*/.thumbnails' \
  --exclude='/tmp/*' \
  --exclude='/lost+found' 
