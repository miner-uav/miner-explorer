#!/usr/bin/env bash

set -e 

EXPERIMENT_NAME=$1
DEST_LOG_PATH=/data/experiment_logs
EXPERIMENT_PATH=${DEST_LOG_PATH}/${EXPERIMENT_NAME}
THIS_PATH="`dirname \"$0\"`"  

DATE_STRING=`date +%Y.%m.%d_%H.%M.%S`
BAG_FILE=/data/rosbag/recorded.bag

if [[ -d  $EXPERIMENT_PATH  ]]; then
	echo "this folder already exists please chose different experiment name."
	exit -1
else
	mkdir -p $EXPERIMENT_PATH
fi

if [[ -z  $EXPERIMENT_NAME  ]]; then
	echo "please supplay experiment name it will be folder name."
	exit -1
fi

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   exit 1
fi

echo "Stopping all services"
set +e
initctl stop miner-px4
initctl stop miner-record

sleep 5

# clean the logs
rm -f /var/log/upstart/miner-*
rm -f $BAG_FILE
set -e

echo "px4"
initctl start miner-px4 

echo "Starting Recording"
initctl start miner-record 

echo proceed with experiment
read -n1 -r -s -p "Press enter to finish..." key

if [ ! $key == $'\x0a'  ]; then
        echo -e "\n Exiting..."
        exit 0
fi

initctl stop miner-px4
initctl stop miner-record

sleep 5
echo "coping bag file: ${EXPERIMENT_PATH}/${EXPERIMENT_NAME}_${DATE_STRING}.bag"
pv ${BAG_FILE} > ${EXPERIMENT_PATH}/${EXPERIMENT_NAME}_${DATE_STRING}.bag
cp -v /var/log/upstart/miner-* ${EXPERIMENT_PATH}/
exit 0












SOURCE_FILE=/data/rosbag/recorded.bag
DEST_PATH=/media/odroid/SAMSUNG_SD/Experiments
EXPERIMENT_ID=$1
EXPERIMENT_PATH=$DEST_PATH/$EXPERIMENT_ID

if [ ! -f $SOURCE_FILE ]; then
    echo "ERROR: Source file does not exists"
    exit -1
fi

if [ ! -d $DEST_PATH ]; then
    echo "ERROR: destination path does not exists: $DEST_PATH"  
    exit -1
fi

if [[ -z "${EXPERIMENT_ID// }" ]]; then
    echo "ERROR: Experiment id can not be empty..."  
    exit -1
fi

if [ -d $EXPERIMENT_PATH ]; then
    echo "ERROR: Path for this experiment already used: $EXPERIMENT_PATH"  
    exit -1
fi

mkdir $DEST_PATH/$EXPERIMENT_ID

DATE_STRING=`date +%Y.%m.%d_%H.%M.%S`

rsync -h --progress $SOURCE_FILE $EXPERIMENT_PATH/${EXPERIMENT_ID}_${DATE_STRING}.bag
ls -lah $EXPERIMENT_PATH/${EXPERIMENT_ID}_${DATE_STRING}.bag
rm $SOURCE_FILE

sudo umount /media/odroid/SAMSUNG_SD
echo "SD CARD Unmounted - you can remove it."

