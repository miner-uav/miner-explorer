#!/usr/bin/env bash

export CAM_LEFT_NAME=/loitor_stereo_visensor/left
export CAM_RIGHT_NAME=/loitor_stereo_visensor/right

rosrun camera_calibration cameracalibrator.py \
--no-service-check \
--size 6x7 \
--square 0.119 \
right:=${CAM_RIGHT_NAME}/image_raw \
right_camera:=${CAM_RIGHT_NAME} \
left:=${CAM_LEFT_NAME}/image_raw \
left_camera:=${CAM_LEFT_NAME}
