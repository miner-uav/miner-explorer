#!/usr/bin/env bash

set -e
HOSTNAME=`hostname`
export ROS_USERNAME=odroid
SOURCE_BASE=/data/git/miner-explorer


## declare an array of services
declare -a SERVICES=(
"miner-test" 
"miner-roscore" 
"miner-px4" 
"miner-sonars" 
"miner-cameras" 
"miner-transforms"
"miner-rovio"
"miner-record"
)

function stopServices {
	echo "stoping service miner-roscore"
	initctl stop miner-roscore
}

function startServices {
   sudo initctl start miner-roscore
}

function copyServiceScripts {
	echo "coping new upstart scripts..."
	for SERVICE in "${SERVICES[@]}"
	do
	   sudo cp -v ${SOURCE_BASE}/odroid/root/etc/init/$SERVICE.conf /etc/init/
	done
}

function setupServices {

	set +e
	stopServices
	
	echo "removing upstart scripts..."
	sudo rm -v /etc/init/miner-*

	echo "removing upstart logs..."
	sudo rm -v /var/log/upstart/miner-*
	set -e
	
	copyServiceScripts

	echo "reloading upstart config"
	sudo initctl reload-configuration

	startServices
}

if [ ! -f ${SOURCE_BASE}/scripts/ros_env_${HOSTNAME}.sh ]; then
	echo "invalid host name; File ${SOURCE_BASE}/scripts/ros_env_${HOSTNAME}.sh does not exists"
	exit -1;
else
	cp ${SOURCE_BASE}/scripts/ros_env_${HOSTNAME}.sh /home/odroid/ros_env.sh
	chown ${ROS_USERNAME}:${ROS_USERNAME} /home/${ROS_USERNAME}/ros_env.sh
fi

setupServices
