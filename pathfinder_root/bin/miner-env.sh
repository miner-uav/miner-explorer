#!/usr/bin/env sh
. /opt/ros/kinetic/setup.sh
. /data/git/miner-tx1/catkin_ws/devel/setup.bash
	
ROS_REMOTE_IP=localhost
ROS_LOCAL_IP=localhost

export ROS_MASTER_URI=http://$ROS_REMOTE_IP:11311/
export ROS_IP=$ROS_LOCAL_IP
export ROS_HOSTNAME=$ROS_LOCAL_IP
export ROS_USERNAME=ubuntu
export ROS_HOME=/data/ros_home
export ROS_BAG=/data/rosbag

