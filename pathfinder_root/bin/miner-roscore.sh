#!/usr/bin/env bash

. /bin/miner-env.sh
# log ros environment
/bin/miner-print-env.sh

# make sure home dir exists
if [ ! -d $ROS_HOME ]; then
	mkdir -p $ROS_HOME
fi
if [ ! -d $ROS_BAG ]; then
	mkdir -p $ROS_BAG
fi
echo "chown $ROS_USERNAME:$ROS_USERNAME $ROS_HOME"
chown $ROS_USERNAME:$ROS_USERNAME $ROS_HOME
echo "chown $ROS_USERNAME:$ROS_USERNAME $ROS_BAG"
chown $ROS_USERNAME:$ROS_USERNAME $ROS_BAG

#start roscore
echo starting ROS core service
roscore
