#!/usr/bin/env bash

. /bin/miner-env-maplab.sh
# log ros environment
/bin/miner-print-env.sh

#start roslaunch
echo "ROS LAUNCH COMMAND:rosrun $ROSRUN_PACKAGE $ROSRUN_FILE $ROSRUN_ARG1 $ROSRUN_ARG2 $ROSRUN_ARG3 $ROSRUN_ARG4 $ROSRUN_ARG5"
rosrun $ROSRUN_PACKAGE $ROSRUN_FILE $ROSRUN_ARG1 $ROSRUN_ARG2 $ROSRUN_ARG3 $ROSRUN_ARG4 $ROSRUN_ARG5

