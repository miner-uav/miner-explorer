#!/usr/bin/env bash
source /opt/ros/kinetic/setup.bash
source /data/git/miner-tx1/catkin_ws/devel/setup.bash

#ROS_REMOTE_IP=localhost
#ROS_LOCAL_IP=localhost

ROS_REMOTE_IP=192.168.8.1
ROS_LOCAL_IP=192.168.8.21

export ROS_MASTER_URI=http://$ROS_REMOTE_IP:11311/
export ROS_IP=$ROS_LOCAL_IP
export ROS_HOSTNAME=$ROS_LOCAL_IP

