#!/usr/bin/env bash

. /bin/miner-env.sh

ROS_TOPIC=$1

TIMEOUT_COUNT=20
SLEEP_INTERVAL=0.5
# timeout in seconds = $TIMEOUT_COUNT * $SLEEP_INTERVAL = 10

#this var is used to detect timeout
TIMEOUT=0

#setting error handling to ignore errors in rostopic command
set +e


until rostopic info $ROS_TOPIC > /dev/null 2>&1;do
    if (( $TIMEOUT_COUNT < 0 )); then
        TIMEOUT=1
        break
    fi
    echo waiting \($TIMEOUT_COUNT\) for $ROS_TOPIC
    sleep $SLEEP_INTERVAL
    ((TIMEOUT_COUNT--))
done

# return error handling to normal
set -e

# check for timeout
if (( $TIMEOUT>0 )); then
   echo timeout waiting for topic: $ROS_TOPIC 
   exit -1
else
   echo topic: $ROS_TOPIC present
fi

