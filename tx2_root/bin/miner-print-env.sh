#!/usr/bin/env bash


echo "********** ROS ENVIRONMENT **********"

echo "RUN USER:                     ${USER}"

echo "ROS_HOME:                     ${ROS_HOME}"

echo "ROS_MASTER_URI:               ${ROS_MASTER_URI}"

echo "ROS_HOSTNAME:                 ${ROS_HOSTNAME}"

echo "ROS_IP:                       ${ROS_IP}"

echo "ROSLISP_PACKAGE_DIRECTORIES:  ${ROSLISP_PACKAGE_DIRECTORIES}"

echo "ROSLISP_PACKAGE_PATH:         ${ROSLISP_PACKAGE_PATH}"

echo "ROS_ETC_DIR:                  ${ROS_ETC_DIR}"

echo "ROS_DISTRO:                   ${ROS_DISTRO}"

echo "ROSLAUNCH_PACKAGE:            ${ROSLAUNCH_PACKAGE}"

echo "ROSLAUNCH_FILE:               ${ROSLAUNCH_FILE}"

echo "ROSLAUNCH_ARGS:               ${ROSLAUNCH_ARGS}"

echo "*************************************"

