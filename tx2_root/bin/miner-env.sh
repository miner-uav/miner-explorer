#!/usr/bin/env sh
. /opt/ros/kinetic/setup.sh
. /data/git/miner-tx1/catkin_ws/devel/setup.bash
	
# this machine IP address
MACHINE_IP=192.168.8.1

export ROS_MASTER_URI=http://127.0.0.1:11311/
export ROS_IP=$MACHINE_IP
export ROS_HOSTNAME=$MACHINE_IP
export ROS_USERNAME=ubuntu
export ROS_HOME=/data/ros_home
export ROS_BAG=/data/rosbag

