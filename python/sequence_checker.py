#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Image
from mavros_msgs.msg import CamIMUStamp

nextSeqImg = 0
nextSeqTs = 0


def check_consistency():
    global nextSeqImg, nextSeqTs
    if abs(nextSeqImg - nextSeqTs) > 1:
        rospy.loginfo("Sequences diverge: Image:" + str(nextSeqImg)
                      + "; TimeStamp:" + str(nextSeqTs) + ";")


def check_ts_seq_cb(ts_data):
    global nextSeqTs
    rospy.loginfo("TS :" + str(ts_data.frame_seq_id))
    if nextSeqTs != ts_data.frame_seq_id:
        rospy.loginfo("Missed Timestamp sequence at " + str(ts_data.frame_seq_id))
    nextSeqTs = ts_data.frame_seq_id + 1
    check_consistency()


def check_image_seq_cb(img_data):
    global nextSeqImg
    rospy.loginfo("IMG:" + str(img_data.header.seq))
    if nextSeqImg != img_data.header.seq:
        rospy.loginfo(rospy.get_caller_id() + " Missed Image sequence at " + str(img_data.header.seq))
    nextSeqImg = img_data.header.seq + 1
    check_consistency()


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('sequence_checker', anonymous=False)

    rospy.Subscriber("/cam0/image_raw", Image, check_image_seq_cb)
    rospy.Subscriber("/px4/cam_imu_sync/cam_imu_stamp", CamIMUStamp, check_ts_seq_cb)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()