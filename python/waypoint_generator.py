#!/usr/bin/env python
import rospy
from tf.transformations import euler_from_quaternion, quaternion_from_euler
from tf import TransformBroadcaster
from sensor_msgs.msg import Joy
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Point, Pose
from math import pi

last_odo = Pose()
start_pose = Pose()
radius = 2

trigger_axes = 7
br = TransformBroadcaster()
centerPointFrame = "tree_scan_center"
startPointFrame = "tree_scan_start"


def joy_cb(joy):
    global start_pose, startPointFrame
    trig_val = joy.axes[trigger_axes]
    if trig_val > 0.5:
        return
    start_pose = last_odo


def odo_cb(odo):
    global last_odo
    last_odo = odo.pose.pose
    [roll, pitch, yaw] = euler_from_quaternion([start_pose.orientation.x, start_pose.orientation.y, start_pose.orientation.z, start_pose.orientation.w])

    q = quaternion_from_euler(0,0,yaw)
    br.sendTransform([start_pose.position.x, start_pose.position.y, start_pose.position.z], q, rospy.Time.now(), startPointFrame, "world")
    br.sendTransform([radius, 0, 0], [0,0,0,1], rospy.Time.now(), centerPointFrame, startPointFrame)


def listener():
    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('waypoint_generator', anonymous=False)

    rospy.Subscriber("/firefly/joy", Joy, joy_cb)
    rospy.Subscriber("/firefly/ground_truth/odometry", Odometry, odo_cb, queue_size=1)

    # spin() simply keeps python from exiting until this node is stopped
    rospy.spin()


if __name__ == '__main__':
    listener()