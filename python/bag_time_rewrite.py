#!/usr/bin/env python
import rosbag
import rospy


class BagRewriter():
    def __init__(self, inBag, topic, timeshift_sec):
        self.outBagFileName = inBag + '.bag'
        self.inBagFileName = inBag
        self.topicName = topic
        self.timeShift = rospy.Duration(int(0), int(timeshift_sec * 1000000000))

    def rewriteBag(self):
        with rosbag.Bag(self.outBagFileName, 'w') as outbag:
            for topic, msg, t in rosbag.Bag(self.inBagFileName).read_messages():
                # This also replaces tf timestamps under the assumption
                # that all transforms in the message share the same timestamp
                if topic == self.topicName:
                    newStamp = msg.header.stamp + self.timeShift
                    msg.header.stamp = newStamp
                    t2 = t + self.timeShift
                    outbag.write(topic, msg, t2)
                else:
                    outbag.write(topic, msg, t)


# Main function.
if __name__ == '__main__':
    # Initialize the node and name it.
    rospy.init_node('bag_rewriter')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        names = rospy.get_param_names()
        inBag = rospy.get_param('~in_bag')
        topic = rospy.get_param('~topic')
        ts = rospy.get_param('~timeshift_sec')

        rewriter = BagRewriter(inBag, topic, ts)
        rewriter.rewriteBag()
    except rospy.ROSInterruptException:
        pass
