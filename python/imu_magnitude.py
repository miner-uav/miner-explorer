#!/usr/bin/env python
import rospy
import array
import math
import csv

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3Stamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler




class ImuGravityMagnitudeChecker():
    def __init__(self):
        self.counter = 0
        self.sampleCount = 10000
        self.gravitySamples = array.array('f', (0.0 for _ in xrange(self.sampleCount)))
        self.acc_bias_x = 0.0
        self.acc_bias_y = 0.0
        self.acc_bias_z = 0.0
        self.use_bias = False
        self.f = open('/home/maciej/gravity.csv', 'wb')
        self.writer = csv.writer(self.f)
        self.writer.writerow(['Time [sec]', 'acc_x', 'acc_y', 'acc_z', 'acc_b_x', 'acc_b_y', 'acc_b_z'])

        rospy.Subscriber("/dji_sdk/imu", Imu, self.imu_cb)
        rospy.Subscriber("/maplab_rovio/bias_acc", Vector3Stamped, self.bias_cb)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.f.close()

    def bias_cb(self, bias_data):
        self.acc_bias_x = bias_data.vector.x
        self.acc_bias_y = bias_data.vector.y
        self.acc_bias_z = bias_data.vector.z

    def imu_cb(self, imu_data):
        gx = imu_data.linear_acceleration.x
        gy = imu_data.linear_acceleration.y
        gz = imu_data.linear_acceleration.z
        #raw = [imu_data.header.stamp.to_sec(), gx, gy, gz, self.acc_bias_x, self.acc_bias_y, self.acc_bias_z]
        #self.writer.writerow(raw)
        if self.use_bias:
            gx -= self.acc_bias_x
            gy -= self.acc_bias_y
            gz -= self.acc_bias_z
        gravity = math.sqrt(gx*gx + gy*gy + gz*gz)
        index = self.counter % self.sampleCount
        self.gravitySamples[index] = gravity
        self.counter += 1

        if self.counter % 200 == 0:
            if self.counter > self.sampleCount:
                gravity_accumul = 0.0
                for gravity in self.gravitySamples:
                    gravity_accumul += gravity
                gravity_avg = gravity_accumul / self.sampleCount
                rospy.loginfo("Gravity Average over last %d samples: %f", self.sampleCount, gravity_avg)
            else:
                rospy.loginfo("Averaging samples %2.0f [%%]", self.counter * 100 / self.sampleCount)
if __name__ == '__main__':
    rospy.init_node('imu_gravity_checker', anonymous=False)
    with ImuGravityMagnitudeChecker() as ip:
        rospy.spin()
