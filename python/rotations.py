#!/usr/bin/python

import minieigen
from tf import transformations
from math import pi

Identity = minieigen.Matrix3(1, 0, 0, 0, 1, 0, 0, 0, 1)

#rot = minieigen.Matrix3(
#      7.7861393635733078e-03, -9.9994068005347003e-01,-7.6165876884214459e-03,
#      -9.9951564258872172e-01,-7.5528715634423094e-03, -3.0189971043402146e-02,
#      3.0130653067371761e-02, 7.8479618596520879e-03,-9.9951515908483568e-01)
rot = minieigen.Matrix3(
       0, -1, 0,
      -1,  0, 0,
       0,  0, -1)

q = minieigen.Quaternion(rot.transpose())
print "WXYZ {} {} {} {} ".format(q[3], q[0], q[1], q[2])

q2 = transformations.quaternion_from_euler(0,-pi/2,0)
print "WXYZ {} {} {} {} ".format(q2[3], q2[0], q2[1], q2[2])
