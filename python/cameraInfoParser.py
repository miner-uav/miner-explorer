#!/usr/bin/env python
import rospy
from sensor_msgs.msg import CameraInfo

leftSubscriber = {}
rightSubscriber = {}






def printCameraInfo(camInfo, camName, topicName):
    rospy.loginfo("*******************************************************")
    rospy.loginfo("CAM INFO: " + camName)
    rospy.loginfo("TOPIC   : " + topicName)

    rospy.loginfo(camName + ".width: " + str(camInfo.width))
    rospy.loginfo(camName + ".height: " + str(camInfo.height))

    Fx = camInfo.K[0]
    Fy = camInfo.K[4]
    Cx = camInfo.K[2]
    Cy = camInfo.K[5]
    rospy.loginfo(camName + ".K.Fx: " + str(Fx))
    rospy.loginfo(camName + ".K.Fy: " + str(Fy))
    rospy.loginfo(camName + ".K.Cx: " + str(Cx))
    rospy.loginfo(camName + ".K.Cy: " + str(Cy))
    if len(camInfo.P) > 0:
        Fx = camInfo.P[0]
        Fy = camInfo.P[5]
        Cx = camInfo.P[2]
        Cy = camInfo.P[6]
        Tx = camInfo.P[3]
        Ty = camInfo.P[7]
        rospy.loginfo(camName + ".P.Fx: " + str(Fx))
        rospy.loginfo(camName + ".P.Fy: " + str(Fy))
        rospy.loginfo(camName + ".P.Cx: " + str(Cx))
        rospy.loginfo(camName + ".P.Cy: " + str(Cy))
        rospy.loginfo(camName + ".P.Tx: " + str(Tx))
        rospy.loginfo(camName + ".P.Ty: " + str(Ty))
    rospy.loginfo(camName + ".distortion_model: " + str(camInfo.distortion_model))
    if len(camInfo.D) > 0:
        rospy.loginfo(camName + ".D.k1: " + str(camInfo.D[0]))
        rospy.loginfo(camName + ".D.k2: " + str(camInfo.D[1]))
        rospy.loginfo(camName + ".D.t1: " + str(camInfo.D[2]))
        rospy.loginfo(camName + ".D.t2: " + str(camInfo.D[3]))
        if len(camInfo.D) > 4:
            rospy.loginfo(camName + ".D.k3: " + str(camInfo.D[4]))
    rospy.loginfo("*******************************************************")
def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('camInfoDump', anonymous=False)
    names = rospy.get_param_names()
    leftTopicName = rospy.get_param("~left_topic", "/zed/left/camera_info")
    rightTopicName = rospy.get_param("~right_topic", "/zed/right/camera_info")

    rospy.loginfo("Waiting for topic: " + leftTopicName)
    msg = rospy.wait_for_message(leftTopicName, CameraInfo)
    printCameraInfo(msg, "LEFT", leftTopicName)

    rospy.loginfo("Waiting for topic: " + rightTopicName)
    msg = rospy.wait_for_message(rightTopicName, CameraInfo)
    printCameraInfo(msg, "RIGHT", rightTopicName)


if __name__ == '__main__':
    listener()