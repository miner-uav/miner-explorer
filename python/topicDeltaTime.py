#!/usr/bin/env python
import rospy
import sys
from rostopic import get_topic_class
import collections
from math import sqrt


def standard_deviation(lst, population=True):
    """Calculates the standard deviation for a list of numbers."""
    num_items = len(lst)
    mean = sum(lst) / num_items
    differences = [x - mean for x in lst]
    sq_differences = [d ** 2 for d in differences]
    ssd = sum(sq_differences)

    # Note: it would be better to return a value and then print it outside
    # the function, but this is just a quick way to print out the values along
    # the way.
    if population is True:
        print('This is POPULATION standard deviation.')
        variance = ssd / num_items
    else:
        print('This is SAMPLE standard deviation.')
        variance = ssd / (num_items - 1)
    sd = sqrt(variance)
    # You could `return sd` here.
    print('The mean is {}.'.format( mean))
    print('The sum of squared differences is {}.'.format(ssd))
    print('The variance is {}.'.format(variance))
    print('The standard deviation is {}.'.format(sd))
    print('--------------------------')



lastTime = rospy.Time()
samples = collections.deque(maxlen=100)

def check_ts_cb(data):
    global lastTime, samples
    deltaTime = data.header.stamp - lastTime
    lastTime = data.header.stamp
    samples.append(deltaTime.to_sec()*1000)
    if len(samples) == 100:
        standard_deviation(samples, False)
        samples.clear()

    # rospy.loginfo("Time since last :" + str(deltaTime.to_sec()*1000) + ' [ms]')


def listener():

    # In ROS, nodes are uniquely named. If two nodes with the same
    # node are launched, the previous one is kicked off. The
    # anonymous=True flag means that rospy will choose a unique
    # name for our 'listener' node so that multiple listeners can
    # run simultaneously.
    rospy.init_node('topicDeltaTimeListner', anonymous=False)
    if len(sys.argv) < 2 :
        sys.exit('please supply topic name as argument');
        return

    topicName = sys.argv[1]
    topics = rospy.get_published_topics("/")
    myTopic = [x for x in topics if x[0] == topicName][0]

    rospy.loginfo("topic name:" + myTopic[0])
    rospy.loginfo("topic type:" + myTopic[1])
    topicClass = get_topic_class(myTopic[0])[0]
    rospy.loginfo("topic class:" + str(topicClass))

    rospy.Subscriber(topicName, topicClass, check_ts_cb)
    rospy.spin()

if __name__ == '__main__':
    listener()