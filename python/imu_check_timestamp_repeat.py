#!/usr/bin/env python
import rospy

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3Stamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler




class ImuTsChecker():
    def __init__(self):
        self.lastTs = rospy.Time(0.0)
        self.lastSeq = 0

        rospy.Subscriber("/loitor_stereo_visensor/imu0", Imu, self.imu_cb)


    def imu_cb(self, imu_data):
        tsDiff = imu_data.header.stamp - self.lastTs
        if tsDiff == 0:
            rospy.loginfo("Repeated Timestamp: %d", imu_data.header.timestmp)
        else:
            if tsDiff.to_sec() < 0.0001:
                rospy.loginfo("T diff: %s", tsDiff)
        self.lastTs = imu_data.header.stamp

        seqDiff = imu_data.header.seq - self.lastSeq
        if not (seqDiff == 1):
            rospy.loginfo("Missing sequences; Last: %d; missed: %d", imu_data.header.seq, seqDiff-1)
        self.lastSeq = imu_data.header.seq

if __name__ == '__main__':
    rospy.init_node('imu_ts_repeat_check', anonymous=False)

    ip = ImuTsChecker()
    rospy.spin()
