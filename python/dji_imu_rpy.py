#!/usr/bin/env python
import rospy

from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3Stamped
from tf.transformations import euler_from_quaternion, quaternion_from_euler




class ImuRepublisher():
    def __init__(self):
        self.rpy_pub = rospy.Publisher('/dji_sdk/imu_rpy', Vector3Stamped, queue_size=1)

        rospy.Subscriber("/dji_sdk/imu", Imu, self.imu_cb)


    def imu_cb(self, imu_data):
        qter = [imu_data.orientation.x, imu_data.orientation.y, imu_data.orientation.z, imu_data.orientation.w]
        rpy = euler_from_quaternion(qter)
        vs = Vector3Stamped()
        vs.header = imu_data.header
        vs.vector.x = rpy[0]
        vs.vector.y = rpy[1]
        vs.vector.z = rpy[2]
        self.rpy_pub.publish(vs)

if __name__ == '__main__':
    rospy.init_node('dji_imu_rpy', anonymous=False)

    ip = ImuRepublisher()
    rospy.spin()
