#!/usr/bin/env python
import rosbag
import rospy
import argparse
import re
import progressbar


class BagRewriter():
    def __init__(self, inBag, outBag, topic_match_pattern, frame_id):
        self.outBagFileName = outBag
        self.inBagFileName = inBag
        self.topicMatchPattern = topic_match_pattern
        self.frameId = frame_id
        self.matchedTopics = []

    def rewriteBag(self):
        matcher = re.compile(self.topicMatchPattern)
        with rosbag.Bag(self.outBagFileName, 'w') as outbag:
            with rosbag.Bag(self.inBagFileName, 'r') as inbag:
                messages = inbag.read_messages()
                pb = progressbar.ProgressBar(max_value=inbag.get_message_count())
                i = 0
                for topic, msg, t in messages:
                    pb.update(i)
                    i += 1
                    # This also replaces tf timestamps under the assumption
                    # that all transforms in the message share the same timestamp
                    if matcher.match(topic):
                        if not topic in self.matchedTopics:
                            self.matchedTopics.append(topic)
                            rospy.loginfo("matched topic: " + topic)
                        msg.header.frame_id = self.frameId
                    outbag.write(topic, msg, t)


# Main function.
if __name__ == '__main__':
    # Initialize the node and name it.
    parser = argparse.ArgumentParser(description='rewrite msg.header.frame_id in rosbag')
    parser.add_argument('inBagFile', metavar='input_file',  help='input bag file')
    parser.add_argument('outBagFile', metavar='oputpu_file',  help='output bag file')
    parser.add_argument('topicMatchPattern', metavar='topic_pattern', help='regex topic match pattern')
    parser.add_argument('frameId', metavar='frame_id', help='new frame id')

    args = parser.parse_args()
    rospy.init_node('bag_rewriter')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        rospy.loginfo("inBagFile: " + "")
        rewriter = BagRewriter(args.inBagFile, args.outBagFile, args.topicMatchPattern, args.frameId)
        rewriter.rewriteBag()
    except rospy.ROSInterruptException:
        pass
