#!/usr/bin/python

# Extract images from a bag file.
#
# Original author: Thomas Denewiler (http://answers.ros.org/users/304/thomas-d/)

# Start up ROS pieces.
BAG_EXTENSION = ".bag"
PKG = 'my_package'
#import roslib; roslib.load_manifest(PKG)
import rosbag
import rospy
import cv2
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError

# Reading bag filename from command line or roslaunch parameter.
import os
import sys

class ImageCreator():

    image_type = ".png"
    desired_topic = "image_rect_color"
    save_dir = ""
    startTime = 0.0
    endTime = 0.0
    bagsStartTime = 0.0
    bagsEndTime = 0.0
    timeInterval = 0.0

    def evaluateBagTimeRange(self, file_name):
        with rosbag.Bag(file_name, 'r') as bag:
            thisBagStartTime = bag.get_start_time()
            thisBagEndTime = bag.get_end_time()

            if self.bagsStartTime == 0.0:
                self.bagsStartTime = thisBagStartTime
            elif self.bagsStartTime > thisBagStartTime:
                self.bagsStartTime = thisBagStartTime

            if self.bagsEndTime == 0.0:
                self.bagsEndTime = thisBagEndTime
            elif self.bagsEndTime < thisBagEndTime:
                self.bagsEndTime = thisBagEndTime

    def parseBag(self, file_name):
        with rosbag.Bag(file_name, 'r') as bag:

            if not (self.endTime >= bag.get_start_time() and self.startTime <= bag.get_end_time()):
                rospy.loginfo("Bag filename: %s, has no data within the requested time boundaries.", file_name)
                return
            else:
                rospy.loginfo("Bag filename: %s, has date within the requested time boundaries.", file_name)
            last_time_stamps = {}
            for topic, msg, t in bag.read_messages():
                topic_parts = topic.split('/')
                # first part is empty string
                last_item = len(topic_parts) -1
                if topic_parts[last_item] == self.desired_topic:
                    camera_name = topic_parts[last_item - 1]
                    if camera_name not in last_time_stamps:
                        last_time_stamps[camera_name] = self.startTime

                    if msg.header.stamp.to_sec() >= (last_time_stamps[camera_name] + self.timeInterval):
                        last_time_stamps[camera_name] = msg.header.stamp.to_sec()
                        try:
                            cv_image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
                        except CvBridgeError, e:
                            print e

                        image_name = self.format_image_name(camera_name, msg.header.stamp)
                        rospy.loginfo("Extracting: %s", image_name)

                        cv2.imwrite(image_name, cv_image, [cv2.IMWRITE_PNG_COMPRESSION, 0])

    def format_image_name(self, camera_name, stamp):
        relativeTs  = stamp.to_sec() - self.bagsStartTime
        image_name = "img-{:08.3f}-{}{}".format(relativeTs, camera_name, self.image_type)
        image_name = os.path.join(self.save_dir, image_name)
        return image_name

    # Must have __init__(self) function for a class, similar to a C++ class constructor.
    def __init__(self):
        # Get parameters when starting node from a launch file.
        if len(sys.argv) < 1:
            self.save_dir = rospy.get_param('save_dir')
            self.bag_dir = rospy.get_param('bag_dir')
            self.startTime = rospy.get_param('start_time')
            self.endTime = rospy.get_param('end_time')
        # Get parameters as arguments to 'rosrun my_package bag_to_images.py <save_dir> <bag_dir>', where save_dir and bag_dir exist relative to this executable file.
        else:
            self.bag_dir = os.path.join(sys.path[0], sys.argv[1])
            self.save_dir = os.path.join(sys.path[0], sys.argv[2])
            self.startTime = float(sys.argv[3])
            self.endTime = float(sys.argv[4])
            self.timeInterval = float(sys.argv[5])
        rospy.loginfo("Bag dir: %s", self.bag_dir)
        rospy.loginfo("Save dir: %s", self.save_dir)
        rospy.loginfo("Time Boundry: %.3f .. %.3f", self.startTime, self.endTime)
        rospy.loginfo("Image interval filtering: %.3f", self.timeInterval)

        # Use a CvBridge to convert ROS images to OpenCV images so they can be saved.
        self.bridge = CvBridge()

        # find start/end time for all bags
        for file in os.listdir(self.bag_dir):
            if file.endswith(BAG_EXTENSION):
                self.evaluateBagTimeRange(os.path.join(self.bag_dir, file))
        rospy.loginfo("Bags times: %.3f .. %.3f", self.bagsStartTime, self.bagsEndTime)

        # extract images from bags
        for file in os.listdir(self.bag_dir):
            if file.endswith(BAG_EXTENSION):
                self.parseBag(os.path.join(self.bag_dir, file))




# Main function.
if __name__ == '__main__':
    # Initialize the node and name it.
    rospy.init_node(PKG)
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        image_creator = ImageCreator()
    except rospy.ROSInterruptException: pass