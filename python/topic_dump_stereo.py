#!/usr/bin/env python
import rospy
import argparse
import yaml
import cv2
import numpy as np
import message_filters
from image_geometry.cameramodels import PinholeCameraModel
from cv_bridge import CvBridge, CvBridgeError
from sensor_msgs.msg import Image, CameraInfo


class TopicStereoDump:
    def __init__(self, out_path, left_topic, right_topic):
        self.topic_left_cam = left_topic
        self.topic_left_cam_image = left_topic + "/image_raw"
        self.topic_left_cam_info = left_topic + "/camera_info"

        self.topic_right_cam = right_topic
        self.topic_right_cam_image = right_topic + "/image_raw"
        self.topic_right_cam_info = right_topic + "/camera_info"

        self.out_path = out_path
        rospy.loginfo("subscribing to image: "  + self.topic_left_cam_image)
        self.l_image_sub = message_filters.Subscriber(self.topic_left_cam_image, Image)

        rospy.loginfo("subscribing to image: " + self.topic_right_cam_image)
        self.r_image_sub = message_filters.Subscriber(self.topic_right_cam_image, Image)

        rospy.loginfo("subscribing to cam info: " + self.topic_left_cam_info)
        self.l_info_sub = message_filters.Subscriber(self.topic_left_cam_info, CameraInfo)

        rospy.loginfo("subscribing to cam info: " + self.topic_left_cam_info)
        self.r_info_sub = message_filters.Subscriber(self.topic_right_cam_info, CameraInfo)

        self.bridge = CvBridge()

        ts = message_filters.TimeSynchronizer([self.l_image_sub, self.l_info_sub, self.r_image_sub, self.r_info_sub], 10)
        ts.registerCallback(self.sync_callback)

    def dump_info(self, filename, camera_name, info_msg):
        m = PinholeCameraModel()
        m.fromCameraInfo(info_msg)
        simple_msg = dict(image_width=m.width,
                          image_height=m.height,
                          camera_name=camera_name,
                          camera_matrix=dict(
                              rows=m.K.shape[0],
                              cols=m.K.shape[1],
                              data=np.array(m.K).reshape(-1).tolist(),
                          ),
                          distortion_model='plumb_bob',
                          distortion_coefficients=dict(
                              rows=m.D.shape[0],
                              cols=m.D.shape[1],
                              data=np.array(m.D).reshape(-1).tolist(),
                          ),
                          rectification_matrix=dict(
                              rows=m.R.shape[0],
                              cols=m.R.shape[1],
                              data=np.array(m.R).reshape(-1).tolist(),
                          ),
                          projection_matrix=dict(
                              rows=m.P.shape[0],
                              cols=m.P.shape[1],
                              data=np.array(m.P).reshape(-1).tolist(),
                          ))
        l_info_yaml = yaml.load(str(simple_msg))
        with open(filename, 'w') as outfile:
            yaml.dump(l_info_yaml, outfile, default_flow_style=False)

    def sync_callback(self, l_img, l_info, r_img, r_info):
        file_name_base = self.out_path + "/bag_" + str(l_img.header.seq)
        try:
            cv_l_img = self.bridge.imgmsg_to_cv2(l_img, "bgr8")
            cv_r_img = self.bridge.imgmsg_to_cv2(r_img, "bgr8")
        except CvBridgeError, e:
            print e
        cv2.imwrite(file_name_base + "_left.png", cv_l_img, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        cv2.imwrite(file_name_base + "_right.png", cv_r_img, [cv2.IMWRITE_PNG_COMPRESSION, 0])
        self.dump_info(file_name_base + "_left_info.yaml", self.topic_left_cam, l_info)
        self.dump_info(file_name_base + "_right_info.yaml", self.topic_right_cam, r_info)




# Main function.
if __name__ == '__main__':
    # Initialize the node and name it.
    parser = argparse.ArgumentParser(description='rewrite msg.header.frame_id in rosbag')
    parser.add_argument('left_cam_topic', metavar='left_cam_topic', help='topic to left camera')
    parser.add_argument('right_cam_topic', metavar='right_cam_topic', help='topic to right camera')
    parser.add_argument('out_path', metavar='out_path', help='where to save the images')

    args = parser.parse_args()
    rospy.init_node('topic_stereo_dump')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        dumper = TopicStereoDump(args.out_path, args.left_cam_topic, args.right_cam_topic)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
