#!/usr/bin/env python
import rospy

from sensor_msgs.msg import PointCloud2
import sensor_msgs.point_cloud2
import math




class FovFromPc2():
    def __init__(self):
        self.minXPoint = tuple([0.0, 0.0, 0.0])
        self.minYPoint = tuple([0.0, 0.0, 0.0])
        self.maxXPoint = tuple([0.0, 0.0, 0.0])
        self.maxYPoint = tuple([0.0, 0.0, 0.0])
        rospy.Subscriber("/gpuimageproc/pointcloud", PointCloud2, self.pc_cb)

    def calc_angle(self, xy, z):
        angle = math.atan2(abs(xy), abs(z)) * 180.0 / math.pi
        return angle

    def pc_cb(self, pc2_data):
        for point in sensor_msgs.point_cloud2.read_points(pc2_data, skip_nans=True):
            # skip far points - unreliable
            if point[2] > 5:
                continue
            # X
            if point[0] > self.maxXPoint[0]:
                self.maxXPoint = point
            if point[0] < self.minXPoint[0]:
                self.minXPoint = point
            # Y
            if point[1] > self.maxYPoint[1]:
                self.maxYPoint = point
            if point[1] < self.minYPoint[1]:
                self.minYPoint = point
            # horizontal
        angleX1 = self.calc_angle(self.minXPoint[0], self.minXPoint[2])
        angleX2 = self.calc_angle(self.maxXPoint[0], self.maxXPoint[2])
        angleX  = angleX1+angleX2;
        angleY1 = self.calc_angle(self.minYPoint[1], self.minYPoint[2])
        angleY2 = self.calc_angle(self.maxYPoint[1], self.maxYPoint[2])
        angleY  = angleY1+angleY2;
        rospy.loginfo("FOV H: %5.2f; V: %5.2f; minX:[%.3f, %.3f]; maxX:[%.3f, %.3f]; minY:[%.3f, %.3f]; maxY:[%.3f, %.3f]"
                      , angleX, angleY
                      , self.minXPoint[0], self.minXPoint[2], self.maxXPoint[0], self.maxXPoint[2]
                      , self.minYPoint[1], self.minYPoint[2], self.maxYPoint[1], self.maxYPoint[2])

if __name__ == '__main__':
    rospy.init_node('dji_imu_rpy', anonymous=False)

    ip = FovFromPc2()
    rospy.spin()
