#!/usr/bin/env python
import rospy
import tf2_ros
import math
from geometry_msgs.msg import TransformStamped
from tf import transformations
from curtsies import Input


class RotatingTransform():
    def __init__(self):
        self.world_frame = 'world'
        self.map_frame = 'map'
        self.rotating_frame = 'uav'

        self.static_broadcaster = tf2_ros.StaticTransformBroadcaster()
        self.broadcaster = tf2_ros.TransformBroadcaster()
        self.transformStamped = self.init_transform(self.map_frame, self.rotating_frame)
        self.radius = 3
        self.height = 1
        self.current_angle = 0
        self.setup_world_map_tf()
        self.rpm = 0.05
        self.iteration_rate = 50.0
        self.angle_step = self.rpm * math.pi * 2.0 / self.iteration_rate

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def setup_world_map_tf(self):
        static_transform_stamped = self.init_transform(self.world_frame, self.map_frame)

        self.static_broadcaster.sendTransform(static_transform_stamped)

    def init_transform(self, frame_src, frame_dst):
        static_transform_stamped = TransformStamped()
        static_transform_stamped.header.stamp = rospy.Time.now()
        static_transform_stamped.header.frame_id = frame_src
        static_transform_stamped.child_frame_id = frame_dst
        static_transform_stamped.transform.translation.x = 0
        static_transform_stamped.transform.translation.y = 0
        static_transform_stamped.transform.translation.z = 0
        static_transform_stamped.transform.rotation.x = 0
        static_transform_stamped.transform.rotation.y = 0
        static_transform_stamped.transform.rotation.z = 0
        static_transform_stamped.transform.rotation.w = 1
        return static_transform_stamped

    def rotate_step(self):
        self.current_angle += self.angle_step
        x = math.cos(self.current_angle) * self.radius
        y = math.sin(self.current_angle) * self.radius
        z = self.height
        yaw = (math.pi / 2.0) + self.current_angle
        q2 = transformations.quaternion_from_euler(0, 0, yaw)

        self.transformStamped.header.stamp = rospy.Time.now()
        self.transformStamped.transform.translation.z = self.height
        self.transformStamped.transform.translation.x = math.cos(self.current_angle) * self.radius
        self.transformStamped.transform.translation.y = math.sin(self.current_angle) * self.radius
        self.transformStamped.transform.rotation.x = q2[0]
        self.transformStamped.transform.rotation.y = q2[1]
        self.transformStamped.transform.rotation.z = q2[2]
        self.transformStamped.transform.rotation.w = q2[3]
        self.broadcaster.sendTransform(self.transformStamped)


if __name__ == '__main__':
    rospy.init_node('uav_circling', anonymous=False)

    with RotatingTransform() as tfm:
        rate = rospy.Rate(tfm.iteration_rate)
        while not rospy.is_shutdown():
            tfm.rotate_step()
            rate.sleep()
