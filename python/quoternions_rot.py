#!/usr/bin/python
import numpy as np

from geometry_msgs.msg import Quaternion
from geometry_msgs.msg import Vector3
from tf.transformations import translation_matrix
from tf.transformations import quaternion_matrix
from tf.transformations import quaternion_from_euler
from math import pi

if __name__ == '__main__':
    qx = 0.7
    qy = 0.7
    qz = 0.7
    qw = 1.0
    quaternion = [qx, qy, qz, qw]
    # or
    roll = -pi / 2
    pitch = 0.0
    yaw = 0.0
    quaternion = quaternion_from_euler(roll, pitch, yaw)
    #quaternion[3] = -quaternion[3]

    tr_x = 0.1017171361476462
    tr_y = 0.08795997125
    tr_z = 0.01895709289717568

    rotation = quaternion_matrix(quaternion)
    translation = translation_matrix([tr_x, tr_y, tr_z])

    transform = np.dot(translation, rotation)
    for i in range(0, 4):
        for j in range(0, 4):
            print "            - %s" % (transform[i, j])
    # print "The quaternion representation is (XYZW) %s %s %s %s." % (qx, qy, qz, qw)
