#!/usr/bin/env python
import rosbag
import rospy
import argparse
import os


class BagRewriter():
    def __init__(self, inBag, outBag, topic, expectedFreq = 16, window = 20):
        self.inBagFileName = inBag
        self.outBagFileName = outBag
        self.topicName = topic
        self.nextTime = rospy.Time()
        self.recNextTime = rospy.Time()
        self.durations = []
        self.window = window
        self.isFirst = True
        self.expectedInterval = rospy.Duration( 1.0 / expectedFreq)
        self.intervalTolerance = rospy.Duration(self.expectedInterval.to_sec() / 10.0)

        for x in range(self.window):
            self.durations.append(self.expectedInterval)

    def checkSample(self, timeStamp, timeRocorded):
        if self.isFirst:
            self.nextTime = timeStamp
            self.recNextTime = timeRocorded
            self.isFirst = False

        avg = self.getAvgDuration()
        recError = timeRocorded - self.recNextTime
        recErrorRelative = recError / avg
        stampError = timeStamp - self.nextTime
        stampErrorRelative = stampError / avg
        if stampErrorRelative < -1.0:
            rospy.logfatal("Advancing time failed at %.6f;", timeStamp)
            self.nextTime = timeStamp
            self.recNextTime = timeRocorded

        # if error is les than 10% then we good
        if stampErrorRelative < 0.1:
            # add new duration to the list
            duration = stampError + avg
            self.durations.append(duration)
            self.durations.pop(0)
            # recalculate the average duration
            avg = self.getAvgDuration()
            # advance the next expected times
            self.nextTime = timeStamp + avg
            self.recNextTime = timeRocorded + avg
            rospy.logdebug("checkSample OK; duration: %.6f; error: %f [%%]; recError: %f [%%]", duration.to_sec(), stampErrorRelative*100.0, recErrorRelative*100.0)
            return timeStamp
        # large error but sample was recorded in correct place - most likely we caught next timestamp because of long exposure time, we use 40% here as it is more erratic
        elif recErrorRelative < 0.4:
            # error bigger than average interval - lets advance lastTime and check again
            ts = self.nextTime
            self.nextTime = self.nextTime + avg
            self.recNextTime = timeRocorded + avg
            rospy.logdebug("checkSample missed timestamp due to long exposure rolling back to: %.6f; error: %f [%%]; recError: %f [%%]", (self.nextTime + avg).to_sec(), stampErrorRelative * 100.0, recErrorRelative*100.0)
            return ts
        else:
            # most likely missing message
            self.nextTime = self.nextTime + avg
            self.recNextTime = self.recNextTime + avg
            rospy.logdebug("checkSample error too large advancing at: %.6f; error: %f", timeStamp.to_sec(), stampErrorRelative * 100.0)
            return self.checkSample(timeStamp, timeRocorded)



    def getAvgDuration(self):
        def add(x, y): return x + y
        sum = reduce(add, self.durations)
        avg = sum / len(self.durations)
        return avg

    def rewriteBag(self):
        rospy.loginfo("STAMP LOG \"SEQ\", \"RECORD TIME\", \"OLD TIME\", \"NEW TIME\", \"DELTA TIME [msec]\"")
        with rosbag.Bag(self.outBagFileName, 'w') as outbag:
            for topic, msg, t in rosbag.Bag(self.inBagFileName).read_messages():
                # This also replaces tf timestamps under the assumption
                # that all transforms in the message share the same timestamp
                if topic == self.topicName:
                    newTime = self.checkSample(msg.header.stamp, t)
                    if not newTime == msg.header.stamp :
                        rospy.loginfo("STAMP LOG, %d, %.9f, %.9f, %.9f, %.9f", msg.header.seq, t.to_sec(), msg.header.stamp.to_sec(), newTime.to_sec(), (newTime - msg.header.stamp).to_sec()*1000.0)
                    msg.header.stamp = newTime
                outbag.write(topic, msg, t)



# Main function.
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='rewrite the bag to fix the duplicate ts')
    parser.add_argument('-i', metavar='bag_file', help='Input bag File Name')
    parser.add_argument('-o', metavar='bag_file', help='Output bag File Name')
    parser.add_argument('-t', metavar='bag_file', help='topic to rewrite')
    args = parser.parse_args()
    if not args.o:
        file, ext = os.path.splitext(args.i)
        args.o =file + ".fixed.bag"

    # Initialize the node and name it.
    rospy.init_node('bag_rewriter')
    # Go to class functions that do all the heavy lifting. Do error checking.
    try:
        rewriter = BagRewriter(args.i, args.o, args.t)
        rewriter.rewriteBag()
    except rospy.ROSInterruptException:
        pass
