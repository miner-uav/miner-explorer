#!/usr/bin/env python
import rospy
import array
import math
import csv

from waypoint_navigator.srv import GoToWaypoints, GoToWaypointsRequest
from geometry_msgs.msg import Point, PoseStamped, Pose
from tf.transformations import euler_from_quaternion, quaternion_from_euler




class RvizTrajectoryGenerator():
    def __init__(self):
        self.counter = 0
        self.currentPoseTopic = '/maplab_rovio/T_M_I'
        self.rvizGoalTopic = '/move_base_simple/goal'
        self.ref_altitude = 2
        self.goToWptsSrv = '/miner/go_to_waypoints'
        self.currentPosition = Pose()
        self.trajectoryPoints = []

        rospy.Subscriber(self.currentPoseTopic, PoseStamped, self.current_position_cb)
        rospy.Subscriber(self.rvizGoalTopic, PoseStamped, self.nav_wpt_cb)

        rospy.wait_for_service(self.goToWptsSrv)
        try:
            self.go_to_wpt_stv_proxy = rospy.ServiceProxy(self.goToWptsSrv, GoToWaypoints)
        except rospy.ServiceException, e:
            print "Service client init failed: %s" % e

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def current_position_cb(self, pose_data):
        self.currentPosition = pose_data.pose

    def nav_wpt_cb(self, pose_data):

        self.trajectoryPoints.append(pose_data.pose.position)
        try:
            points = [self.currentPosition.position]
            points.extend(self.trajectoryPoints)
            gtw_msg = GoToWaypointsRequest(points)
            response = self.go_to_wpt_stv_proxy(gtw_msg)
            rospy.loginfo("Response: %s", str(response))
        except rospy.ServiceException, e:
            print "Service call failed: %s" % e


if __name__ == '__main__':
    rospy.init_node('rviz_traj_gen', anonymous=False)
    with RvizTrajectoryGenerator() as ip:
        rospy.spin()
