#!/usr/bin/env python
import rospy
import tf2_ros
import threading
from geometry_msgs.msg import Vector3
from math import sqrt
from curtsies import Input


class TransformMeasurements():
    def __init__(self):
        self.src_frame = 'mission'
        self.target_frame = 'imu'
        self.tfBuffer = tf2_ros.Buffer()
        self.tfListener = tf2_ros.TransformListener(self.tfBuffer)
        self.lastTransform = Vector3()
        self.lastKey = u'0'

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def print_current_dist(self):
        try:
            trans = self.tfBuffer.lookup_transform(self.src_frame, self.target_frame, rospy.Time())
            thisTranslation = trans.transform.translation
            dist = sqrt((thisTranslation.x - self.lastTransform.x) ** 2 + (thisTranslation.y - self.lastTransform.y) ** 2 + (thisTranslation.z - self.lastTransform.z) ** 2)
            rospy.loginfo("Distance: %s" % dist)
        except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
            rospy.logerr("Failed to get transform: %s" % e)

    def save_current_position(self):
        if self.lastKey == u's':
            self.lastKey = u'0'
            try:
                trans = self.tfBuffer.lookup_transform(self.src_frame, self.target_frame, rospy.Time())
                self.lastTransform = trans.transform.translation
            except (tf2_ros.LookupException, tf2_ros.ConnectivityException, tf2_ros.ExtrapolationException) as e:
                rospy.logerr("Failed to get transform: %s" % e)

    def read_keys_thread(self):
        with Input(keynames='curses') as input_generator:
            for e in input_generator:
                self.lastKey = e
                if e == u'q':
                    return
                print(repr(e))

if __name__ == '__main__':
    rospy.init_node('transform_measurements', anonymous=False)
    rate = rospy.Rate(4.0)
    with TransformMeasurements() as tfm:
        thread = threading.Thread(target=tfm.read_keys_thread)
        thread.start()
        while not rospy.is_shutdown():
            tfm.print_current_dist()
            tfm.save_current_position()
            if tfm.lastKey == u'q':
                break
            rate.sleep()
        thread.join()