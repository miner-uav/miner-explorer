#!/usr/bin/env python
import rospy
import time
from std_msgs.msg import Float32
from sensor_msgs.msg import Imu
from geometry_msgs.msg import Vector3,Vector3Stamped
from math import sqrt




class ImuRepublisher():
    def __init__(self):
        self.N = 800
        self.variance_threshold = 0.0009
        self.data_index = 0
        self.acc_data = [Vector3() for i in range(self.N)]
        self.acc_var_data = [Vector3() for i in range(self.N)]
        self.imu_topic = '/loitor_stereo_visensor/imu0'
        #self.imu_topic = '/dji_sdk/imu'

        self.imu_subs = rospy.Subscriber(self.imu_topic, Imu, self.imu_cb)
        self.variance_pub = rospy.Publisher('/imu_variance/imu_variance', Float32, queue_size=1)


    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        return

    def imu_cb(self, imu_data):
        start = time.time()
        self.acc_data[self.data_index] = imu_data.linear_acceleration
        self.data_index += 1
        if self.data_index >= self.N:
            self.data_index = 0
        accumulator = Vector3()
        for sample in self.acc_data:
            accumulator.x += sample.x
            accumulator.y += sample.y
            accumulator.z += sample.z
        accumulator.x /= self.N
        accumulator.y /= self.N
        accumulator.z /= self.N

        variance = Vector3()
        for i in range(self.N):
            variance.x += (self.acc_data[i].x - accumulator.x)**2
            variance.y += (self.acc_data[i].y - accumulator.y)**2
            variance.z += (self.acc_data[i].z - accumulator.z)**2
        variance.x /= self.N
        variance.y /= self.N
        variance.z /= self.N
        variance_mag = sqrt(variance.x**2 + variance.y**2 + variance.z**2)
        end = time.time()
        vs = Float32()
        vs.data = variance_mag
        self.variance_pub.publish(vs)
        if variance_mag > self.variance_threshold:
            rospy.loginfo_throttle(0.1, "Movement   in %f [us]" % ((end-start)*1000000))
        else:
            rospy.loginfo_throttle(0.1, "Stationary in %f [us]" % ((end-start)*1000000))

if __name__ == '__main__':
    rospy.init_node('imu_variance', anonymous=False)

    with ImuRepublisher() as ip:
        rospy.spin()
